from lxml import html
import requests
import json

'''
Extract all hrefs from the page and dump to json
'''

response = requests.get('http://www.auriga.com')
full_html = html.fromstring(response.content.decode())

output = []
for node in full_html.xpath('.//a'):
    output.append({'text': node.text, 'link': node.xpath('@href')[0]})

with open('output/links.json', 'w') as f:
    f.write(json.dumps(output, sort_keys=True, indent=4, separators=(',', ': ')))
