import re
import subprocess

'''
Calculate number of connections for each PID throw 'lsof' command
'''

proc = subprocess.Popen('lsof', stdout=subprocess.PIPE)

proc_stat = {}
for line in iter(proc.stdout.readline, b''):
    try:
        pid = int(re.findall('\w+', line.decode('utf8'))[1])
    except ValueError:
        continue

    if not proc_stat.get(pid):
        proc_stat[pid] = 1
    else:
        proc_stat[pid] += 1

print('{}\t{}'.format('PID', 'Connections'))
for k, v in proc_stat.items():
    print('{}\t{}'.format(k, v))
