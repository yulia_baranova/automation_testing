import re
from datetime import datetime

REGEXPS = {'start': re.compile(r'\[(.+)\] INFO Started processing of >([\w\s\d]+)<'),
           'finish': re.compile(r'\[(.+)\] INFO Finished processing of >([\w\s\d]+)<'),
           'error': re.compile(r'.+ ERROR ([\w\s]+) for >([\w\s\d]+)<')}

errors = {}
processing = {}

with open('output/task_1_2.log', 'r') as log:
    for line in log.readlines():
        try:
            event, match = next(filter(lambda x: x[1],[(k, re.match(v, line)) for k, v in REGEXPS.items()]))
        except (ValueError, StopIteration):
            continue

        data, request = match.groups()
        if event == 'start':
            processing[request] = datetime.strptime(data, '%Y-%m-%d %H:%M:%S.%f')
        elif event == 'finish':
            time = datetime.strptime(data, '%Y-%m-%d %H:%M:%S.%f') - processing[request]
            processing[request] = '{}.{}'.format(time.seconds, str(time.microseconds)[:3])
        elif event == 'error':
            errors[request] = data



for k, v in processing.items():
    print('{}: {}'.format(k, v if isinstance(v, str) else '-1'))

for k, v in errors.items():
    print('{}: {}'.format(k, v))
